import socket

PORT = 6060
SERVER = socket.gethostbyname(socket.gethostname())
ADDR = (SERVER,PORT)
FORMAT = 'utf-8'
MAX_SIZE = 4096
END_CHAR = '\0'

def send_msg(msg):
    print 'Enviando \"{0}\" al servidor {1}'.format(msg,ADDR)
    client_socket.sendto(msg.encode(FORMAT),ADDR)
    try:
        data,addr = client_socket.recvfrom(MAX_SIZE)
    except socket.timeout:
        print "[ERROR] No se pudo procesar, el servidor tardo mucho en responder\n" 
        return
    print 'El servidor {0} dice: \"{1}\"\n'.format(addr,data)

client_socket = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
client_socket.settimeout(10)
msg = " "
while(msg!=""):
    print 'Escriba un mensaje de hasta {0} caracteres y presione Enter para enviar. Deje en blanco y presione Enter para salir'.format(MAX_SIZE)
    msg = raw_input()
    if(msg!=""):
        send_msg(msg)

print 'Terminado'
client_socket.close()
