import socket

PORT = 6060
SERVER = socket.gethostbyname(socket.gethostname())
ADDR = (SERVER,PORT)
FORMAT = 'utf-8'
MAX_SIZE = 4096
END_CHAR = '\n'

server_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
server_socket.bind(ADDR)

def start_server():
    print 'Server escuchando en {0}'.format(ADDR)
    while True:
        data,addr = server_socket.recvfrom(4096)
        print 'Recibido \"{0}\" desde {1}'.format(data,addr)
        mensaje_a_enviar = data.upper()
        print 'Enviando \"{0}\" a {1}'.format(mensaje_a_enviar,addr)
        server_socket.sendto(mensaje_a_enviar.encode(FORMAT),addr)

start_server()