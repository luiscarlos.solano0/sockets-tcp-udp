import socket
import sys

# TCP/IP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
FORMAT = 'utf-8'
# Conectar el socket al puerto
server_address = ('localhost', 10000)
print >>sys.stderr, 'conectando al %s puerto %s' % server_address
sock.connect(server_address)

try:
    # Enviar datos
    msn = " "
    while (msn != ""):
        print 'Escriba un mensaje de hasta {0} caracteres y presione Enter para enviar. Deje en blanco y presione Enter para salir'.format(4096)
        msn = raw_input()
        if (msn != ""):
            sock.sendto(msn.encode(FORMAT), server_address)
            print >> sys.stderr, 'enviando "%s"' % msn
            # Buscando respuesta
            amount_received = 0
            amount_expected = len(msn)

            while amount_received < amount_expected:
                datos = sock.recv(4096)
                amount_received += len(datos)
                print >> sys.stderr, 'recibido "%s"' % datos

finally:
    print >> sys.stderr, 'cerrando socket'
    sock.close()
