import socket
import sys

# TCP/IP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
FORMAT = 'utf-8'
# Puerto unido al socket
server_address = ('localhost', 10000)
print >>sys.stderr, 'comienza en %s el puerto %s' % server_address
sock.bind(server_address)

# Buscar las senales proximos
sock.listen(1)

while True:
    # Esperar conexion
    print >>sys.stderr, 'esperando conexion'
    connection, client_address = sock.accept()

    try:
        print >> sys.stderr, 'connexion de', client_address

        # Recibe los datos
        while True:
            data = connection.recv(4096)
            msn=data.upper()
            print >> sys.stderr, 'recibido "%s"' % data
            if data:
                print >> sys.stderr, 'enviando datos al cliente'
                connection.sendto(msn.encode(FORMAT), client_address)
            else:
                print >> sys.stderr, 'no hay mas datos de', client_address
                break
    finally:
        # Limpia la conexion
        connection.close()
